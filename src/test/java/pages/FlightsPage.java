package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import runner.RunCucumber;

import static support.Commands.*;

public class FlightsPage extends RunCucumber{

    // Link Principal
    private String URL = "https://phptravels.net/";

    // Opções: Voos - Hotéis - Tours - Serviços
    private By vAbaVoos = By.xpath("//button[contains(.,'Flights')]");

    // Aba Voos
    private By vTabGroup = By.id("tab-group-events");
    private By vRadioGroupIdaVolta = By.id("round-trip");
    private By vSelectOrigem = By.xpath("//span[contains(@data-select2-id,'2')]");
    private By vBotaoCidadeOrigem = By.xpath("//button[contains(.,'LHE')]");
    private By vSelectDestino = By.xpath("//span[@data-select2-id='5']");
    private By vBotaoCidadeDestino = By.xpath("//button[contains(.,'DXB')]");
    private By vBotaoProcurarViagens = By.xpath("//button[contains(@id,'flights-search')]");
    private By vDataIda = By.id("departure");
    private By vDataRetorno = By.id("return_date");

    // Voos pesquisados
    private By vFiltros = By.id("fadein");
    private By vFiltrosPakistan = By.id("oneway_flights_0");
    private By vFiltroZerado = By.xpath("//img[@alt='no results']");

    // Fluxo Principal
    public void verificaAbaAtual(){
        // Identificar se está na aba "Flights"
        WebElement abaVoos = getDriver().findElement(vAbaVoos);

        if (!abaVoos.isEnabled()){
            clickElement(vAbaVoos);
        }
    }

    public void limpaDatas(){
        // Encontrar o elemento do campo de data
        WebElement campoDataInicio = getDriver().findElement(vDataIda);

        for (int i = 0; i < 10; i++) {
            campoDataInicio.sendKeys(Keys.BACK_SPACE);
        }

        WebElement dataRetorno = getDriver().findElement(vDataRetorno);
        for (int i = 0; i < 10; i++) {
            dataRetorno.sendKeys(Keys.BACK_SPACE);

        }

    }
    public void acessarAplicao() {
        getDriver("chrome").get(URL);
        getDriver().manage().window().maximize();
        waitElementBeVisible(vTabGroup, 5);

    }

    public void pesquisaVoos(){
       verificaAbaAtual();

       clickElement(vSelectOrigem);
       clickElement(vBotaoCidadeOrigem);

       clickElement(vSelectDestino);
       clickElement(vBotaoCidadeDestino);

        // Encontrar o elemento do campo de data
        WebElement campoData = getDriver().findElement(vDataIda);

        for (int i = 0; i < 10; i++) {
            campoData.sendKeys(Keys.BACK_SPACE);
        }

       fillField(vDataIda, "06-10-2023");
       clickElement(vBotaoProcurarViagens);
    }

    public void verificaVoos(){
        waitElementBeVisible(vFiltros, 15);
        clickElement(vFiltrosPakistan);
    }

    // Alternativo
    public void pesquisaVooInexistente(){
        verificaAbaAtual();
        clickElement(vSelectDestino);
        clickElement(vBotaoCidadeDestino);

        // Encontrar o elemento do campo de data
        WebElement campoData = getDriver().findElement(vDataIda);

        for (int i = 0; i < 10; i++) {
            campoData.sendKeys(Keys.BACK_SPACE);
        }

        fillField(vDataIda, "06-10-2023");
        clickElement(vBotaoProcurarViagens);
    }

    public void verificaFaltaVoos(){
        waitElementBeClickable(vFiltroZerado, 15);
    }

    // Excecao
    public void pesquisaIdaVolta(){
        verificaAbaAtual();
        clickElement(vRadioGroupIdaVolta);
        clickElement(vSelectOrigem);
        clickElement(vBotaoCidadeOrigem);

        clickElement(vSelectDestino);
        clickElement(vBotaoCidadeDestino);
        limpaDatas();

        fillField(vDataIda, "06-10-2023");
        fillField(vDataRetorno, "07-10-2023");
        clickElement(vBotaoProcurarViagens);

    }

}
