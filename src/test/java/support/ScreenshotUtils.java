package support;

import cucumber.api.Scenario;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import static runner.RunBase.getDriver;

import java.io.File;
import java.io.IOException;


public class ScreenshotUtils {
    public static void addScreenshotOnScenario(Scenario scenario) {

        System.out.println("========================");
        System.out.println("Teste que está sendo executado: " + scenario.getName());
        System.out.println("Status: " + scenario.getStatus());
        System.out.println("TagName: " + scenario.getSourceTagNames());
        System.out.println("========================");

        if (scenario.isFailed()) {

            // A foto está em tipo array Byte, pego os dados e coloco na minha variável
            byte[] screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);

            // Aqui eu pego esse Byte e o converto em imagem passando os parâmetros
            scenario.embed(screenshot, "image/png");
        }
    }

    public static void addScreenshot(Scenario scenario) {

        // Mesmo contexto da screenshot anterior, mas esse independe de status do teste.
        byte[] screenshot = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
        scenario.embed(screenshot, "image/png");
    }
    public static class SeleniumTakeScreenshot{
        public static void fotos() throws IOException{
            File scrFile = ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File("C:/automacao-portal-solarview-2.0/target/formated-report/attachments/image.png"));
        }
    }
    
}