package support;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.openqa.selenium.WebDriver;
import runner.RunCucumber;
import java.util.Random;


public class Commands extends RunCucumber {

    static WebDriver driver;

    //Esperar Elemento estar presente na tela
    public static void waitElementBeClickable(By element, Integer tempo) {
        WebDriverWait wait = new WebDriverWait(getDriver(), tempo);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static void waitElementBeVisible(By element, Integer tempo) {
        WebDriverWait wait = new WebDriverWait(getDriver(), tempo);
        wait.until(ExpectedConditions.visibilityOfElementLocated(element));
    }

    //Esse random gera nome com números aleatorios
    public static String getRandomNomeNumero() {

        String email_init = "User";
        String email_final = "Test";


        Random random = new Random();
        int minimo = 1;
        int maximo = 999999999;
        int resultado = random.nextInt(maximo - minimo) + minimo;

        return email_init + resultado + email_final;

    }

    //Esse random gera endereços eletrónicos aleatorios
    public static String getRandomEmail() {

        String email_init = "teste_";
        String email_final = "@hotmail.com";


        Random random = new Random();
        int minimo = 1;
        int maximo = 999999999;
        int resultado = random.nextInt(maximo - minimo) + minimo;

        return email_init + resultado + email_final;

    }

    //Desce até o final da página
    public static void scrolldown() throws InterruptedException {
        JavascriptExecutor jse = (JavascriptExecutor) driver;
        jse.executeScript("window.scrollTo(0, 10000)");
        Thread.sleep(2000);

    }

    //Esse comando serve para clicar em um campo
    public static void clickElement(By element) {
        System.out.println("#############################################");

        try {
            System.out.println("Vai clicar no elemento: " + element);
            waitElementBeClickable(element, 1000);
            getDriver().findElement(element).click();
            System.out.println("Clicou no elemento: " + element);

        } catch (Exception error) {
            System.out.println("********** Ocorreu um erro ao tentar clicar no elemento: " + element);
            System.out.println(error);

        }

        System.out.println("#############################################");

    }

    //Esse comando serve para preecher em um campo
    public static void fillField(By element, String value) {
        System.out.println("#############################################");

        try {
            System.out.println("Vai preencher o campo: " + element);
            waitElementBeVisible(element, 1000);
            getDriver().findElement(element).sendKeys(value);
            System.out.println("Preencheu o campo: " + element);

        } catch (Exception error) {
            System.out.println("********** Ocorreu um erro ao preencher o campo: " + element);
            System.out.println(error);

        }

        System.out.println("#############################################");

    }

    public static void checkMensage(By element, String expectedMessage) {
        String actualMessage = "";
        System.out.println("#############################################");
        System.out.println("Vai validar a mensagem: " + expectedMessage);
        waitElementBeVisible(element, 10000);
        actualMessage = getDriver().findElement(element).getText();

        Assert.assertEquals("Erro ao validar mensagens!", expectedMessage, actualMessage);

        System.out.println("Validou a mensagem: " + expectedMessage);
        System.out.println("#############################################");

    }


}
