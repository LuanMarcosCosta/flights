package steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.pt.Dado;
import cucumber.api.java.pt.Entao;
import cucumber.api.java.pt.Quando;
import pages.FlightsPage;
import runner.RunCucumber;
import support.ScreenshotUtils;

public class VoosSteps extends RunCucumber {
    FlightsPage flights = new FlightsPage();

    // FLUXO PRINCIPAL
    @Dado("^que estou na tela de voos$")
    public void telaVoos() {
        flights.acessarAplicao();
    }

    @Quando("^seleciono um voo com passageiros$")
    public void selecinaVoos()  {
        flights.pesquisaVoos();
    }

    @Entao("^vejo voos disponiveis$")
    public void voosDisponiveis()  {
        flights.verificaVoos();
    }

    // Cenário de Voo Inexistente - ALTERNATIVO
    @Quando("^seleciono um voo inexistente$")
    public void selecionaVooInexistente() {
        flights.pesquisaVooInexistente();
    }

    @Entao("^vejo resultado sem voos$")
    public void vejoVooInexistente() {
        flights.verificaFaltaVoos();
    }

    // Teste de EXCEÇÃO
    @Quando("^seleciono um voo com ida e volta$")
    public void vooIdaVolta()  {
        flights.pesquisaIdaVolta();
    }

    @Entao("^vejo resultados$")
    public void vejoResultados()  {
        flights.verificaFaltaVoos();
    }

    @After
    public static void takeScreenshot(Scenario scenario){
            ScreenshotUtils.addScreenshot(scenario);
    }

}
