# language: pt
Funcionalidade: Voos
  Eu como QA - automatizando
  Quero acessar a pagina de voos
  Para verificar quais disponiveis

  Contexto:
    Dado que estou na tela de voos

  @Voo-sucesso  @regressivo
    Cenario: Pesquisa de Voos com sucesso
    Quando seleciono um voo com passageiros
    Então vejo voos disponiveis

  @Voo-insucesso  @regressivo
    Cenario: Pesquisa de voo inexistentes
    Quando seleciono um voo inexistente
    Entao vejo resultado sem voos

  @Voo-alternativo  @regressivo
    Cenario: Pesquisa de voo com ida e volta de dois dias
    Quando seleciono um voo com ida e volta
    Entao vejo resultados
